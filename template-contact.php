<?php
/*
Template Name: Contact
*/
?>

<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <?php
    // Grab custom variables
    $banner_bg_url = get_field('banner-image') != '' ? get_field('banner-image') : get_template_directory_uri().'/assets/build/images/banner-default.jpg';
    $banner_title = get_field('banner-title') != ''  ? get_field('banner-title') : get_the_title();
    ?>
    <div class="banner-wrap" style="background-image: url('<?php echo $banner_bg_url; ?>');">
        <div class="banner">
            <h2><?php echo $banner_title; ?></h2>
        </div>
    </div>
    <div id="content">
        <div class="secondary-page-wrap">
            <div class="contact-wrap">
                <div class="col">
                    <?php the_content(); ?>
                    <?php echo do_shortcode('[contact-form-7 id="251" title="Contact Us"]'); ?>
                </div>
                <div class="col map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11855.330498552536!2d-88.08923073174671!3d42.02532287921061!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880faf27fc867875%3A0xa49303ce2dafbeb7!2sTrickster+Gallery!5e0!3m2!1sen!2sus!4v1509032630152" width="500" height="400" frameborder="0" style="border:0" allowfullscreen></iframe></div>
            </div>
            <div class="clearer"> </div>
        </div>
    </div>

<?php endwhile; ?>

<?php get_footer(); ?>