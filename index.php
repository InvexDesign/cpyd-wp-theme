<?php get_header(); ?>

<?php include('includes/home-slider.php'); ?>

<div id="content">
	<div class="callout-container">
		<div class="callout">
			<div class="icon" style="background-color: #3F4895">
				<i class="fa fa-flag" aria-hidden="true"></i>
			</div>
			<span class="title" style="color: #3F4895">Drug Take Back</span>
			<p class="content">Download this list of <a href="<?php echo home_url('/'); ?>wp-content/uploads/Drug-Take-Back-Locations-2017-181.pdf" target="_blank">Drug Take Back Locations</a> or click the link below to learn more about this national effort.</p>
			<a href="https://takebackday.dea.gov/" style="color: #3F4895">Learn More &raquo;</a>
		</div>
		<div class="callout">
			<div class="icon" style="background-color: #A5C43C">
				<i class="fa fa-users" aria-hidden="true"></i>
			</div>
			<span class="title" style="color: #A5C43C">FY18 Meetings</span>
			<p class="content">10am - 12pm<br />
				<em>Trickster Native Art Gallery</em><br />
				190 S Roselle Rd, Schaumburg, IL<br />
				<strong>Sep 27 - Nov 15 - Jan 24 - Apr 25 - May 16</strong>
			</p>
			<a href="<?php echo home_url('/'); ?>contact" style="color: #A5C43C">Full Schedule &raquo;</a>
		</div>
		<div class="callout">
			<div class="icon" style="background-color: #5BC0B6">
				<i class="fa fa-newspaper-o" aria-hidden="true"></i>
			</div>
			<span class="title" style="color: #5BC0B6">Newsletters</span>
			<p class="content">Stay up to date on what's going on both with CPYD and in the community with our latest newsletters.</p>
			<a href="<?php echo home_url('/'); ?>newsletters" style="color: #5BC0B6">Read Newsletters  &raquo;</a>
		</div>
		<div class="callout">
			<div class="icon" style="background-color: #F7584C">
				<i class="fa fa-file-text-o" aria-hidden="true"></i>
			</div>
			<span class="title" style="color: #F7584C">Resource Guide</span>
			<p class="content">Our interactive resource guide, available for both adults &amp; teens, makes it easy to find the help that you're looking for.</p>
			<a href="<?php echo home_url('/'); ?>resource-guide" style="color: #F7584C">Find Help Today &raquo;</a>
		</div>
	</div>
	<div class="clearer"></div>
</div>

<?php get_footer(); ?>
