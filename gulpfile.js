var gulp = require('gulp'),
    concat = require('gulp-concat'),
    deporder = require('gulp-deporder'),
    stripdebug = require('gulp-strip-debug'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    assets = require('postcss-assets'),
    autoprefixer = require('autoprefixer'),
    mqpacker = require('css-mqpacker'),
    cssnano = require('cssnano'),
    newer = require('gulp-newer'),
    imagemin = require('gulp-imagemin'),
    devBuild = (process.env.NODE_ENV !== 'production'),// development mode?
    folder = {
        source: 'assets/source/',
        build: 'assets/build/',
        node: 'node_modules/'
    }
;

gulp.task('js', function()
{
    var jsbuild = gulp.src([
            folder.node + 'jquery/dist/jquery.min.js',
            folder.node + 'responsivemultilevelmenu/js/modernizr.custom.js',
            folder.node + 'responsivemultilevelmenu/js/jquery.dlmenu.js',
            folder.node + 'jquery.cycle2/src/jquery.cycle2.min.js',
            folder.source + 'js/**/*'
        ])
        .pipe(deporder())
        .pipe(concat('bundle.min.js'));

    if(!devBuild)
    {
        jsbuild = jsbuild
            .pipe(stripdebug())
            .pipe(uglify());
    }

    return jsbuild.pipe(gulp.dest(folder.build + 'js/'));
});

gulp.task('images', function()
{
    var out = folder.build + 'images/';
    return gulp.src(folder.source + 'images/**/*')
        .pipe(newer(out))
        .pipe(imagemin({optimizationLevel: 5}))
        .pipe(gulp.dest(out));
});

gulp.task('fonts', function()
{
    var out = folder.build + 'fonts/';
    return gulp.src([
            folder.node + 'font-awesome/fonts/*',
            folder.node + 'responsivemultilevelmenu/fonts/*',
            folder.source + 'fonts/*'
        ])
        .pipe(gulp.dest(out));
});

gulp.task('css', ['images'], function()
{
    var postCssOpts = [
        assets({loadPaths: ['images/']}),
        autoprefixer({browsers: ['last 2 versions', '> 2%']}),
        mqpacker
    ];

    if(!devBuild)
    {
        postCssOpts.push(cssnano);
    }

    return gulp.src([
            folder.node + 'font-awesome/css/font-awesome.min.css',
            folder.node + 'responsivemultilevelmenu/css/component.css',
            folder.source + 'sass/style.scss'
        ])
        .pipe(sass({
            outputStyle: 'nested',
            imagePath: 'images/',
            precision: 3,
            errLogToConsole: true
        }))
        .pipe(concat('app.min.css'))
        .pipe(postcss(postCssOpts))
        .pipe(gulp.dest(folder.build + 'css/'));
});

gulp.task('watch', function()
{
    gulp.watch(folder.source + 'images/**/*', ['images']);
    gulp.watch(folder.source + 'js/**/*', ['js']);
    gulp.watch(folder.source + 'sass/**/*', ['css']);
});

gulp.task('run', ['fonts', 'css', 'js']);
gulp.task('default', ['run']);