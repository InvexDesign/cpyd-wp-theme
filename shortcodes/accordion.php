<?php

function accordionShortcode($params, $content = null)
{
	extract(shortcode_atts([
		'autoclose'    => true,
		'openfirst'    => false,
		'openall'      => false,
		'clicktoclose' => false,
		'class'        => ''
	], $params));

	$classes = isset($class) ? $class : '';
	$classes .= isset($autoclose) ? ' autoclose' : '';
	$classes .= isset($openfirst) ? ' openfirst' : '';
	$classes .= isset($openall) ? ' openall' : '';
	$classes .= isset($clicktoclose) ? ' clicktoclose' : '';

	return
		"<div class='accordion $classes'>" .
			do_shortcode($content) .
		"</div>";
}
add_shortcode('accordion', 'accordionShortcode');

function accordionItemShortcode($params, $content = null)
{
	extract(shortcode_atts([
		'title' => 'Accordion Title',
		'class' => '',
		'open'  => false
	], $params));

	$id = preg_replace('/\s+/', '_', strtolower($title));
	$checked = $open ? 'checked="checked"' : '';

	return
		"<div class='accordion-item $class'>" .
			"<input id='accordion_item__$id' type='checkbox' $checked />" .
			"<label for='accordion_item__$id'>$title</label>" .
			"<div class='container'><div class='content'>$content</div></div>" .
		"</div>";
}
add_shortcode('accordion-item', 'accordionItemShortcode');