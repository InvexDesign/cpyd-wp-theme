<?php

function tabsShortcode($params, $content = null)
{
	extract(shortcode_atts([
		'class' => ''
	], $params));

	$classes = isset($class) ? $class : '';

	return
		"<div class='tabs $classes'>" .
		"<div class='labels'></div>" .
			do_shortcode($content) .
		"</div>";
}
add_shortcode('tabs', 'tabsShortcode');

function tabShortcode($params, $content = null)
{
	extract(shortcode_atts([
		'title' => 'Tab Title',
		'class' => '',
	], $params));

	$id = preg_replace('/\s+/', '_', preg_replace('/[^\sA-Za-z0-9]/', '', strtolower($title)));

	return "
		<label id='tab_label__$id' data-tab='tab_content__$id'>$title</label>
		<div id='tab_content__$id' class='tab $class'>
			<h4 class='print-title'>$title</h4>
			<div class='content'>$content</div>
		</div>
	";
}
add_shortcode('tab', 'tabShortcode');