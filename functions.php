<?php
require_once('includes/constants.php');
require_once('includes/HeaderMenuWalker.php');
require_once('includes/MobileMenuWalker.php');
require_once('shortcodes/accordion.php');
require_once('shortcodes/tabs.php');

add_theme_support('menus');
add_theme_support('post-thumbnails');

function dd($stuff)
{
	var_export($stuff);
	die;
}

function enqueueAssets()
{
	wp_register_style('app-css', get_template_directory_uri() . '/assets/build/css/app.min.css');
	wp_enqueue_style('app-css');

	wp_register_script('app-js', get_template_directory_uri() . '/assets/build/js/bundle.min.js');
	wp_enqueue_script('app-js');
}

add_action('wp_enqueue_scripts', 'enqueueAssets');

function getMetaSlides($metaslider_id)
{
	$args = [
		'force_no_custom_order' => true,
		'orderby'               => 'menu_order',
		'order'                 => 'ASC',
		'post_type'             => ['attachment', 'ml-slide'],
		'post_status'           => ['inherit', 'publish'],
		'lang'                  => '', // polylang, ingore language filter
		'suppress_filters'      => 1, // wpml, ignore language filter
		'posts_per_page'        => -1,
		'tax_query'             => [
			[
				'taxonomy' => 'ml-slider',
				'field'    => 'slug',
				'terms'    => $metaslider_id
			]
		]
	];

	$slides = [];
	$args = apply_filters('metaslider_populate_slides_args', $args, $metaslider_id, []);
	$query = new WP_Query($args);
	while($query->have_posts())
	{
		$query->next_post();
		$post = $query->post;
		$featured_image = get_the_post_thumbnail_url($post->ID);
		$slides[] = [
			'image_url' => $featured_image ?: $post->guid,
			'title'     => get_post_meta($post->ID, 'ml-slider_title', true),
			'content'   => $post->post_excerpt,
			'link'      => get_post_meta($post->ID, 'ml-slider_url', true),
			'button'    => get_post_meta($post->ID, '_wp_attachment_image_alt', true)
		];
	}
	wp_reset_postdata();

	return $slides;
}

function preProcessMenuItems($menu_id)
{
	$menu = [];
	$menu_items = wp_get_nav_menu_items($menu_id);

	foreach($menu_items as $menu_item)
	{
		$item = [
			'post_id' => get_post_meta($menu_item->ID, '_menu_item_object_id', true),
			'name'    => $menu_item->title,
			'url'     => $menu_item->url,
			'target'  => $menu_item->target,
			'classes' => implode(' ', $menu_item->classes),
		];

		if($parent_id = $menu_item->menu_item_parent)
		{
			if(isset($menu[$parent_id]))
			{
				if(!isset($menu[$parent_id]['children']))
				{
					$menu[$parent_id]['children'] = [$menu_item->ID => $item];
				}
				else
				{
					$menu[$parent_id]['children'][$menu_item->ID] = $item;
				}
			}
			else
			{
				//more than one level of sub menus
			}
		}
		else
		{
			$menu[$menu_item->ID] = $item;
		}
	}

	return $menu;
}

function displayMultilevelResponsiveMenu($menu_id)
{
	$menu = preProcessMenuItems($menu_id);
	include('includes/responsive-menu.php');
}