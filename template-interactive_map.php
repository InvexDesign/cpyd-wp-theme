<?php
/*
Template Name: Interactive Map
*/
?>

<?php get_header(); ?>

<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

	<?php
	require(LARAVEL_DIRECTORY . '/bootstrap/autoload.php');
	$app = require_once(LARAVEL_DIRECTORY . '/bootstrap/app.php');
	$kernel = $app->make('Illuminate\Contracts\Http\Kernel');
	$kernel->handle($request = Illuminate\Http\Request::capture());
	$__env = $app->make('Illuminate\View\Factory');

	$services = \App\Models\Service::all();
	$service_options = \App\Models\Service::all()->pluck('name', 'id')->all();
	$facilities = \App\Models\Facility::all();
	$age_group_options = \App\Models\Options\AgeGroup::all()->pluck('name', 'id')->all();

	$filter_url = FILTER_URL;
	$process_facility_click_url = PROCESS_FACILITY_CLICK_URL;
	?>

	<link href="<?php echo LARAVEL_URL; ?>/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo LARAVEL_URL; ?>/css/select2.min.css" rel="stylesheet">
	<link href="<?php echo LARAVEL_URL; ?>/css/select-two-custom.css" rel="stylesheet">
	<link href="<?php echo LARAVEL_URL; ?>/css/featherlight.min.css" rel="stylesheet">
	<link href="<?php echo LARAVEL_URL; ?>/css/interactive-map.css" rel="stylesheet">
	<link href="<?php echo LARAVEL_URL; ?>/css/lightbox.css" rel="stylesheet">

	<script src="<?php echo LARAVEL_URL; ?>/js/bootstrap.min.js"></script>
	<script src="<?php echo LARAVEL_URL; ?>/js/select2.full.min.js"></script>
	<script src="<?php echo LARAVEL_URL; ?>/js/featherlight.min.js"></script>
	<script src="<?php echo LARAVEL_URL; ?>/js/helpers.js"></script>
	<script src="<?php echo LARAVEL_URL; ?>/js/utilities.js"></script>

	<script>
			function filterFacilities()
      {
          closeOpenInfoWindow();

          $('div.loader').show();

          var data = {
              search: $('#search_filter').val(),
              service_ids: $('#services_filter').val(),
              age_group_ids: $('#age_group_filter').val()
          };
          var jqxhr = $.post("<?php echo e($filter_url); ?>", data)
              .done(function(response)
              {
                  var active_results_count = 0;
                  var total_results_count = 0;
                  var json = JSON.parse(response);
                  $.each(json.facilities, function(id, is_visible)
                  {
                      if(FACILITIES[id] && FACILITIES[id].marker)
                      {
                          FACILITIES[id].marker.setVisible(is_visible);
                          $('#facility_list_' + id).toggle(is_visible);

                          if(is_visible)
                          {
                              active_results_count++;
                          }

                          total_results_count++
                      }
                  });
                  $('#active_results_count').html(active_results_count);
                  $('#total_results_count').html(total_results_count);
              }).fail(function(response)
              {
                  console.error("ERROR!");
                  console.error(response);
              }).always(function()
              {
                  $('div.loader').hide();
              });
      }

      $(document).ready(function()
      {
          $('#services_filter').select2({
              placeholder: 'All Offered Services'
          });
          $('#age_group_filter').select2({
              placeholder: 'All Ages (Adult & Teen)'
          });

          $('.filter').change(function()
          {
              filterFacilities();
          });
      });
	</script>

	<?php
	// Grab custom variables
	$banner_bg_url = get_field('banner-image') != '' ? get_field('banner-image') : get_template_directory_uri().'/assets/build/images/banner-default.jpg';
	$banner_title = get_field('banner-title') != ''  ? get_field('banner-title') : get_the_title();
	?>
	<div class="banner-wrap interactive-map" style="background-image: url('<?php echo $banner_bg_url; ?>');">
		<div class="banner">
			<h2><?php echo $banner_title; ?></h2>
		</div>
	</div>

	<?php echo $__env->make('frontend.partials.interactive_map_content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div id="content" class="content interactive-map">
	  <?php echo get_the_content(); ?>
	</div>

<?php endwhile; endif; ?>

<?php get_footer(); ?>