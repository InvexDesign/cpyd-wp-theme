jQuery(document).ready(function($)
{
    $('.dl-menu .has-children > a').each(function(index, element)
    {
        var parent = $(element);
        var href = parent.attr('href');
        var innerHTML = element.innerHTML;
        var children = parent.siblings('.dl-submenu');
        var firstRealChild = children.find('li.dl-back:lt(1)');
        $('<li class="inserted"><a href="' + href + '">' + innerHTML + '</a></li>').insertAfter(firstRealChild);
    });
});
