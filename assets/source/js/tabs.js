jQuery(document).ready(function($)
{
    $('.tabs label').each(function(index, element)
    {
        var labels = $(element).closest('.tabs').find('.labels');
        $(element).clone().appendTo(labels);
        $(element).remove();
    });
    $('.tabs .labels label').click(function()
    {
        $('.tabs label').removeClass('active');
        $('.tabs .tab').removeClass('active');

        $(this).addClass('active');

        var tab_id = $(this).data('tab');
        $('#' + tab_id).addClass('active');
    });
    $('.tabs label').first().addClass('active');
    $('.tabs .tab').first().addClass('active');
    $('.tabs > br').remove();
    $('.tabs .tab .content').each(function(index, element)
    {
        $(element).find('br').first().remove();
    })
});