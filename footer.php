		<div id="footer">
			<div class="ribbon">
				<div class="content">
					<ul class="links">
						<li><a href="tel:18472854518"><i class="fa fa-mobile fa-lg" aria-hidden="true"></i> &nbsp;(847) 285-4520</li>
						<li class="separator">::</li>
						<li><a href="mailto:cpyd.coalition@gmail.com"><i class="fa fa-envelope-o fa-lg" aria-hidden="true"></i> &nbsp;cpyd.coalition@gmail.com</a></li>
						<li class="separator">::</li>
						<li class="meetings">Meetings @ Trickster Native Art Gallery &nbsp;&mdash;&nbsp; <a href="https://goo.gl/maps/ZpdxLuobpkn" target="_blank"><i class="fa fa-map-marker fa-lg" aria-hidden="true"></i> &nbsp;190 S. Roselle Rd, Schaumburg, IL 60193</a></li>
					</ul>
					<ul class="social-media">
						<li><a href="<?php echo FACEBOOK_URL; ?>" target="_blank"><img src="<?php echo get_template_directory_uri()?>/assets/build/images/social_facebook.png"></a></li>
						<li><a href="<?php echo TWITTER_URL; ?>" target="_blank"><img src="<?php echo get_template_directory_uri()?>/assets/build/images/social_twitter.png"></a></li>
                        <li><a href="https://www.linkedin.com/in/cpyd-coalition-979056160/" target="_blank"><img src="<?php echo get_template_directory_uri()?>/assets/build/images/social_linkedin.png"></a></li>
						<!--<li><a href="<?php echo GOOGLE_PLUS_URL; ?>"><img src="<?php echo get_template_directory_uri()?>/assets/build/images/social_googleplus.png"></a></li>-->
					</ul>
				</div>
			</div>
            <div class="sub-footer">
                <div class="left-sub">
                    <a href="https://www.kennethyoung.org" title="Kenneth Young Center" target="_blank"><img src="<?php echo get_template_directory_uri() . '/assets/build/images/kyc-logo.png';?>" alt="Kenneth Young Center Logo"/></a>
                </div>
                <div class="right-sub">
                    <span class="bottom">Communities for Positive Youth Development &copy; <?php echo date("Y"); ?> |&nbsp;Powered By <a class="invex-link" href="https://www.invexdesign.com" target="_blank" title="Chicago Web Design, Web Hosting, SEO | Invex Design"><strong>Invex Design</strong></a></span>
                    <span class="disclaimer">Funding provided in whole or in part to the Kenneth Young Center by the Illinois Department of Human Services (IDHS), Substance Abuse and Mental Health Services Administration (SAMHSA), and the Office of Adolescent Health (OAH).</span>
                </div>
            </div>
		</*div>
	</div>

	<?php wp_footer(); ?>
</body>
</html>
