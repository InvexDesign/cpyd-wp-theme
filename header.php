<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title('|', true, 'right'); ?></title>

    <?php include(get_template_directory() . '/includes/favicon.php'); ?>

	<?php wp_head(); ?>
</head>
<body>
<div id="wrapper">
	<div id="header">
		<div class="top">
			<h1 class="logo">
				<a href="<?php echo home_url('/'); ?>" title="<?php wp_title(); ?>">
					<img src="<?php echo get_template_directory_uri() . '/assets/build/images/logo.png';?>" alt="Logo"/>
					<img src="<?php echo get_template_directory_uri() . '/assets/build/images/logo-text.png';?>" class="logo-text" alt="Logo Text"/>
				</a>
			</h1>
			<div class="right">
				<img class="purpose-quote" src="<?php echo get_template_directory_uri() . '/assets/build/images/purpose-quote.png';?>" alt="Purpose Quote"/>
				<span class="locations">Schaumburg, Hoffman Estates, Palatine, Elk Grove Township, Hanover Park</span>
			</div>
		</div>
		<div class="nav-container">
			<ul class="nav">
				<?php
				wp_nav_menu([
					'menu' => HEADER_NAV_MENU_ID,
					'walker'         => new HeaderMenuWalker(),
					'container'      => false,
					'items_wrap'     => '%3$s'
				]);
				?>
			</ul>
			<?php displayMultilevelResponsiveMenu(RESPONSIVE_MENU_ID); ?>
		</div>
	</div>