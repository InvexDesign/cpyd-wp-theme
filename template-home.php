<?php
/*
Template Name: Home
*/
?>

<?php get_header(); ?>

<?php include('includes/home-slider.php'); ?>

<div id="content">
	<div class="callout-container">
	  <?php if(have_rows('callouts')) : while(have_rows('callouts')) : the_row(); ?>
		  <?php
				if(!get_sub_field('enabled')) {
					continue;
				}
				$link = get_sub_field('link');
		  ?>
				<div class="callout">
					<div class="icon" style="background-color: <?php echo get_sub_field('color'); ?>;">
						<i class="fa <?php echo get_sub_field('icon'); ?>" aria-hidden="true"></i>
					</div>
					<span class="title" style="color: <?php echo get_sub_field('color'); ?>;">
							<?php echo get_sub_field('title'); ?>
					</span>
					<p class="content"><?php echo get_sub_field('content'); ?></p>
					<a href="<?php echo get_sub_field('link')['url']; ?>"
						 style="color: <?php echo get_sub_field('color'); ?>;"
			  		<?php echo $link['target'] ? 'target="' . $link['target'] . '"' : ''; ?>><?php echo $link['title']; ?> &raquo;</a>
				</div>
	  <?php endwhile; endif; ?>
	</div>
	<div class="clearer"></div>
</div>

<?php get_footer(); ?>
