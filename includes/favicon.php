<?php
    $favicon_dir = get_template_directory_uri() . '/favicon';
    ?>
<!-- Favicon Includes-->
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $favicon_dir; ?>/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $favicon_dir; ?>/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $favicon_dir; ?>/favicon-16x16.png">
<link rel="manifest" href="<?php echo $favicon_dir; ?>/manifest.json">
<link rel="mask-icon" href="<?php echo $favicon_dir; ?>/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#ffffff">
<!-- END: Favicon -->