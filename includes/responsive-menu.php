<div id="responsive_menu">
	<div id="dl-menu" class="responsive-menu dl-menuwrapper">
		<button class="dl-trigger"><i class="fa fa-bars fa-lg" aria-hidden="true"></i> &nbsp;Menu</button>
		<ul class="dl-menu JAZZ">
		<?php
		wp_nav_menu([
			'menu' => HEADER_NAV_MENU_ID,
			'walker'         => new MobileMenuWalker(),
			'container'      => false,
			'items_wrap'     => '%3$s'
		]);
		?>
		</ul>
	</div>
</div>
<div class="clearer"></div>