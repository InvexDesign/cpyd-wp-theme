<?php
/*
Template Name: Newsletters
*/
?>

<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <?php
    // Grab custom variables
    $banner_bg_url = get_field('banner-image') != '' ? get_field('banner-image') : get_template_directory_uri().'/assets/build/images/banner-default.jpg';
    $banner_title = get_field('banner-title') != ''  ? get_field('banner-title') : get_the_title();
    ?>
    <div class="banner-wrap" style="background-image: url('<?php echo $banner_bg_url; ?>');">
			<div class="banner">
				<h2><?php echo $banner_title; ?></h2>
			</div>
    </div>
    <div id="content">
			<div class="secondary-page-wrap">
				<?php if(have_rows('newsletters')) : ?>
					<div class="newsletters">
						<?php while(have_rows('newsletters')) : the_row(); ?>
							<?php
							$newsletter = [
								'title'       => get_sub_field('title'),
								'description' => get_sub_field('description'),
								'tooltip'     => get_sub_field('tooltip'),
								'image'       => get_sub_field('image')['url'],
								'background'  => get_sub_field('background'),
								'link'        => get_sub_field('link'),
								'active'      => get_sub_field('active'),
							];
							?>
							<?php if($newsletter['active']) : ?>
								<a href="<?php echo $newsletter['link']['url']; ?>"
									 class="img-caption-slide-up-hover-effect"
									 <?php echo $newsletter['link']['target'] ? 'target="' . $newsletter['link']['target'] . '"' : ''; ?>>
									<img src="<?php echo $newsletter['image']; ?>" <?php echo $newsletter['tooltip'] ? 'title="' . $newsletter['tooltip'] . '"' : ''; ?>>
									<div class="caption" style="background-color: <?php echo $newsletter['background']; ?>;">
										<h2><?php echo $newsletter['title']; ?></h2>
										<?php echo "<p>" . $newsletter['description'] . "</p>"; ?>
									</div>
								</a>
							<?php endif; ?>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>

			  <?php the_content(); ?>

				<div class="clearer"> </div>
			</div>
    </div>

<?php endwhile; ?>

<?php get_footer(); ?>