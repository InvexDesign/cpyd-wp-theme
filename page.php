<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <?php
        // Grab custom variables
        $banner_bg_url = get_field('banner-image') != '' ? get_field('banner-image') : get_template_directory_uri().'/assets/build/images/banner-default.jpg';
        $banner_title = get_field('banner-title') != ''  ? get_field('banner-title') : get_the_title();
    ?>
    <div class="banner-wrap" style="background-image: url('<?php echo $banner_bg_url; ?>');">
        <div class="banner">
            <h2><?php echo $banner_title; ?></h2>
        </div>
    </div>
    <div id="content">
        <div class="secondary-page-wrap">
            <?php the_content(); ?>
            <div class="clearer"> </div>
        </div>
    </div>

<?php endwhile; ?>

<?php get_footer(); ?>
